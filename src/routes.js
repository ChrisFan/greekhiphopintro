import homepage from './pages/homepage.vue'
import detailpage from './pages/detailpage.vue'
import categories from './components/categories.vue'



export default [
  { path: '/', component: homepage },
  { path: '/categories', component: categories },
  { name: 'detailPage', path: '/detailpage/:id', component: detailpage },
]
